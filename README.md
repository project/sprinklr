# Sprinklr

The Sprinklr modules provides sprinklr chatbot integration to your site.

The module allows you to integrate sprinklr chatbot with provided "App Id".

The sprinklr chatbot can be enabled for specific pages.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sprinklr).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sprinklr).


## Table of contents

 - Requirements
 - Installation
 - Configuration
 - Override Settings


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.

For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit /admin/config/system/sprinklr to configure.
2. Uncheck the "Enable sprinklr chatbot" checkbox to disable the feature.
3. Enter "App Id" to connect with sprinklr chatbot.
   For Multilingual Site - Add translations for "App Id"
   `/admin/config/system/sprinklr/translate` if different App Ids are required.
4. Configure URLs list to enable sprinklr chatbot on specific pages or to
   disable it on some pages.
5. Choose content types to enable it for all the nodes of chosen content types.
6. Click "Save configuration" to apply your changes.


## Override Settings

It is possible to override chatbot feature by implementing an event listener
like the example below:

```
document.addEventListener('sprChatSettingsAlter', (e) => {
  const sprChatSettings = e.detail;
  // Set userName value in sprChatSettings variable.
  sprChatSettings.userName = 'John';
});
```

## Maintainers

- Rohit Joshi - [joshi.rohit100](https://www.drupal.org/u/joshirohit100)
- Manish Sharma - [manish-31](https://www.drupal.org/u/manish-31)
- Wouter Adem - [wouter.adem](https://www.drupal.org/u/wouteradem)
