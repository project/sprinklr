<?php

namespace Drupal\Tests\sprinklr\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Covers the access of SprinklrSettingsForm.
 *
 * @group sprinklr
 */
class SprinklrPermissionTest extends BrowserTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'sprinklr',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the sprinklr configuration form access.
   */
  public function testConfigFormAccess(): void {
    // Check for user having sprinklr permission.
    $adminUser = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($adminUser);
    $this->drupalGet('/admin/config/services/sprinklr');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();

    // Check for user not having sprinklr permission.
    $normalUser = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($normalUser);
    $this->drupalGet('/admin/config/services/sprinklr');
    $this->assertSession()->statusCodeEquals(403);

    // Check for anonymous user for not having access.
    $this->drupalGet('/admin/config/services/sprinklr');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();
  }

}
