<?php

namespace Drupal\Tests\sprinklr\Unit;

use Drupal\sprinklr\Helper\SprinklrHelper;
use Drupal\system\Tests\Routing\MockAliasManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\sprinklr\Helper\SprinklrHelper
 * @group sprinklr
 */
class SprinklrHelperTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock current patch matcher.
    $this->currentPath = $this->createMock(CurrentPathStack::class);
    // Mock alias manager.
    $this->aliasManager = new MockAliasManager();
    // Mock path matcher.
    $this->pathMatcher = $this->createMock(PathMatcherInterface::class);
    // Mock route matcher service.
    $this->routeMatch = $this->createMock(RouteMatchInterface::class);
  }

  /**
   * Provide test data for ::testIsSprinklrEnabled().
   */
  public function providerTestIsSprinklrEnabled() {
    return [
      [TRUE, '1224', TRUE],
      [TRUE, '', FALSE],
      [FALSE, '123', FALSE],
      [FALSE, '', FALSE],
      ['', '', FALSE],
    ];
  }

  /**
   * Tests whether sprinklr enabled or not.
   *
   * @covers ::isSprinklrFeatureEnabled
   * @dataProvider providerTestIsSprinklrEnabled
   */
  public function testIsSprinklrEnabled($sprinklr_status, $appId, $expected): void {
    $config_factory = $this->getConfigFactoryStub([
      'sprinklr.settings' => [
        'sprinklr_enabled' => $sprinklr_status,
        'app_id' => $appId,
      ],
    ]);

    $sprinklrHelper = new SprinklrHelper(
      $config_factory,
      $this->currentPath,
      $this->aliasManager,
      $this->pathMatcher,
      $this->routeMatch,
    );
    $sprinklrEnabled = $sprinklrHelper->isSprinklrFeatureEnabled();
    $this->assertEquals($expected, $sprinklrEnabled);
  }

  /**
   * Provide test data for ::testSprinklrEnabledForNodeType().
   */
  public function providerTestSprinklrEnabledForNodeType() {
    return [
      [
        'Drupal\node\NodeInterface',
        'page',
        'entity.node.canonical',
        'node',
        TRUE,
      ],
      [
        'Drupal\node\NodeInterface',
        'article',
        'entity.node.canonical',
        'node',
        FALSE,
      ],
      [
        'Drupal\taxonomy\TermInterface',
        'tags',
        'entity.taxonomy.canonical',
        'taxonomy',
        FALSE,
      ],
    ];
  }

  /**
   * Tests sprinklr available for configured node type page.
   *
   * @covers ::isSprinklrEnabledForCurrentNode
   * @dataProvider providerTestSprinklrEnabledForNodeType
   */
  public function testSprinklrEnabledForNodeType(
    $entity_class,
    $entity_bundle,
    $entity_view_route,
    $entity_route_param,
    $expected_output,
  ): void {
    $config_factory = $this->getConfigFactoryStub([
      'sprinklr.settings' => [
        'allowed_content_types' => [
          'page' => 'page',
          'article' => '0',
        ],
      ],
    ]);

    $entity = $this->getMockBuilder($entity_class)
      ->disableOriginalConstructor()
      ->getMock();

    $entity->method('bundle')
      ->willReturn($entity_bundle);

    $this->routeMatch->method('getParameter')
      ->with($entity_route_param)
      ->willReturn($entity);

    $this->routeMatch->method('getRouteName')
      ->willReturn($entity_view_route);

    $sprinklrHelper = new SprinklrHelper(
      $config_factory,
      $this->currentPath,
      $this->aliasManager,
      $this->pathMatcher,
      $this->routeMatch,
    );
    $sprinklrEnabled = $sprinklrHelper->isSprinklrEnabledForCurrentNode();
    $this->assertEquals($expected_output, $sprinklrEnabled);
  }

}
