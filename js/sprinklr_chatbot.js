/**
 * @file
 * Initialize Sprinklr chatbot.
 */

(function (drupalSettings) {
  window.sprChatSettings = window.sprChatSettings || {};
  const sprChatSettings = {
    appId: drupalSettings.sprinklr.appId,
    skin: drupalSettings.sprinklr.skin,
  };

  // Allow other modules to alter sprinklr chat settings.
  document.dispatchEvent(new CustomEvent('sprChatSettingsAlter', {
      detail: sprChatSettings,
    })
  );
  window.sprChatSettings = sprChatSettings;
})(drupalSettings);
